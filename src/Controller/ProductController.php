<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\PriceRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_index")
     *
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param PriceRepository $priceRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(
        Request $request,
        ProductRepository $productRepository,
        PaginatorInterface $paginator,
        PriceRepository $priceRepository
    )
    {
        $query = $productRepository->getAllDql(false);

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $this->getParameter('app.pagination_limit')
        );

        $prices = $priceRepository->getLastPrices($pagination->getItems());

        return $this->render('product/index.html.twig', [
            'pagination' => $pagination,
            'prices' => $prices
        ]);
    }

    /**
     * @Route("/product/update-ajax", name="product_update_ajax")
     *
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param PriceRepository $priceRepository
     * @return JsonResponse
     */
    public function indexAjaxAction(
        Request $request,
        ProductRepository $productRepository,
        PaginatorInterface $paginator,
        PriceRepository $priceRepository
    )
    {
        $message = 'success';

        try {
            $query = $productRepository->getAllDql(false);

            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                $this->getParameter('app.pagination_limit')
            );

            $pagination->setUsedRoute('product_index');

            $prices = $priceRepository->getLastPrices($pagination->getItems());

            $productsBlock = $this->render('product/components/products_block.html.twig', [
                'pagination' => $pagination,
                'prices' => $prices
            ])->getContent();

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse(['message' => $message, 'productsBlock' => $productsBlock]);
    }

    /**
     * @Route("/product/show-partner", name="product_show_partner")
     *
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param PriceRepository $priceRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showPartnerAction(
        Request $request,
        ProductRepository $productRepository,
        PaginatorInterface $paginator,
        PriceRepository $priceRepository)
    {
        $query = $productRepository->getAllDql(true);

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $this->getParameter('app.pagination_limit')
        );

        $prices = $priceRepository->getLastPrices($pagination->getItems());

        return $this->render('product/show_partner.html.twig', [
            'pagination' => $pagination,
            'prices' => $prices
        ]);
    }

    /**
     * @Route("/product/partner-update-ajax", name="product_partner_update_ajax")
     *
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param PriceRepository $priceRepository
     * @return JsonResponse
     */
    public function showPartnerAjaxAction(
        Request $request,
        ProductRepository $productRepository,
        PaginatorInterface $paginator,
        PriceRepository $priceRepository
    )
    {
        $message = 'success';

        try {
            $query = $productRepository->getAllDql(true);

            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                $this->getParameter('app.pagination_limit')
            );

            $pagination->setUsedRoute('product_show_partner');

            $prices = $priceRepository->getLastPrices($pagination->getItems());

            $productsBlock = $this->render('product/components/products_block.html.twig', [
                'pagination' => $pagination,
                'prices' => $prices
            ])->getContent();

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse(['message' => $message, 'productsBlock' => $productsBlock]);
    }

    /**
     * @Route("/product/category/{id}", name="product_category")
     *
     * @param Category $category
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param PriceRepository $priceRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCategoryAction(
        Category $category,
        Request $request,
        ProductRepository $productRepository,
        PaginatorInterface $paginator,
        PriceRepository $priceRepository
    )
    {
        $query = $productRepository->getByCategoryDQL($category);

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $this->getParameter('app.pagination_limit')
        );

        $prices = $priceRepository->getLastPrices($pagination->getItems());

        return $this->render('product/category.html.twig', [
            'pagination' => $pagination,
            'prices' => $prices,
            'category' => $category
        ]);
    }

    /**
     * @Route("/product/category-ajax/{id}", name="product_category_ajax")
     *
     * @param Category $category
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param PriceRepository $priceRepository
     * @return JsonResponse
     */
    public function showCategoryAjaxAction(
        Category $category,
        Request $request,
        ProductRepository $productRepository,
        PaginatorInterface $paginator,
        PriceRepository $priceRepository
    )
    {
        $message = 'success';

        try {
            $query = $productRepository->getByCategoryDQL($category);

            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                $this->getParameter('app.pagination_limit')
            );

            $pagination->setUsedRoute('product_category', ['id' => $category->getId()]);

            $prices = $priceRepository->getLastPrices($pagination->getItems());

            $productsBlock = $this->render('product/components/products_block.html.twig', [
                'pagination' => $pagination,
                'prices' => $prices,
                'category' => $category
            ])->getContent();

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse(['message' => $message, 'productsBlock' => $productsBlock]);
    }

    /**
     * @Route("/product/{id}", name="product_show")
     *
     * @param Product $product
     * @param PriceRepository $priceRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showProductAction(Product $product, PriceRepository $priceRepository)
    {
        $price = $priceRepository->getLastPrice($product);

        return $this->render('product/product.html.twig', [
            'product' => $product,
            'price' => $price
        ]);
    }
}
