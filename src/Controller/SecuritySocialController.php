<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ULoginRegisterType;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class SecuritySocialController extends AbstractController
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/login-social", name="security_social_login_social")
     * @Method("POST")
     *
     * @param Request $request
     * @param UserHandler $userHandler
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginSocialAction(Request $request, UserHandler $userHandler, UserRepository $userRepository)
    {
        $token = $request->request->get('token');

        if ($token) {
            $s = file_get_contents('http://ulogin.ru/token.php?token='
                . $token . '&host=' . $request->server->get('HTTP_HOST'));
            $userData = json_decode($s, true);

            if (isset($userData['network']) && isset($userData['uid'])) {
                $user = $userRepository->findBySocialId($userData['network'], $userData['uid']);

                if ($user) {
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('main_index');
                }
            }
        }

        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Route("/register-social-start", name="security_social_register_social_start")
     * @Method("POST")
     *
     * @param Request $request
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerSocialStartAction(Request $request, SessionInterface $session)
    {
        $token = $request->request->get('token');

        if ($token) {
            $s = file_get_contents('http://ulogin.ru/token.php?token='
                . $token . '&host=' . $request->server->get('HTTP_HOST'));
            $userData = json_decode($s, true);

            if (isset($userData['network']) && isset($userData['uid'])) {
                $user = new User();
                $user->setEmail($userData['email']);

                $form = $this->createForm(ULoginRegisterType::class, $user, [
                    'action' => $this->generateUrl('security_social_register_social_end')
                ]);

                $session->set(self::ULOGIN_DATA, $userData);

                return $this->render('security_social/register.html.twig', [
                    'form' => $form->createView()
                ]);
            }
        }

        return $this->redirectToRoute('fos_user_registration_register');
    }

    /**
     * @Route("/register-social-end", name="security_social_register_social_end")
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerSocialEndAction(
        Request $request,
        SessionInterface $session,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = $session->get(self::ULOGIN_DATA);

        $user = new User();
        $form = $this->createForm(ULoginRegisterType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user
                ->setPlainPassword($userData['uid'])
                ->setRoles(['ROLE_USER'])
                ->setEnabled(true)
                ->setSocialId($userData['network'], $userData['uid']);

            $manager->persist($user);
            $manager->flush();

            $userHandler->makeUserSession($user);

            return $this->redirectToRoute('main_index');
        }

        return $this->render('security_social/register.html.twig', [
            'form' => $form->createView()
        ]);
    }
}