<?php

namespace App\Controller\Profile;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 *
 * Class MainController
 * @package App\Controller\Profile
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="profile_main_index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('profile/main/index.html.twig');
    }
}