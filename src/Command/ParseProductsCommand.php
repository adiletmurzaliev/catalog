<?php

namespace App\Command;

use App\Model\Parser\PhoneParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ParseProductsCommand extends Command
{
    protected static $defaultName = 'app:parse-products';
    /**
     * @var PhoneParser
     */
    private $phoneParser;

    public function __construct(PhoneParser $phoneParser)
    {
        parent::__construct();

        $this->phoneParser = $phoneParser;
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse products');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->phoneParser->updateData();

        $io->success('Products have been successfully parsed!');
    }
}
