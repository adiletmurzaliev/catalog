<?php

namespace App\Repository;

use App\Entity\Price;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use PhpParser\Node\Expr;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Price|null find($id, $lockMode = null, $lockVersion = null)
 * @method Price|null findOneBy(array $criteria, array $orderBy = null)
 * @method Price[]    findAll()
 * @method Price[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Price::class);
    }

    public function getLastPrices(array $products)
    {
        $ids = [];
        foreach ($products as $product) {
            $ids[] = $product->getId();
        }

        try {
            $sql = '
            SELECT 
                pr.*, IF(p.discount IS NULL, NULL, (100 - p.discount)/100 * pr.amount) AS discount_amount
            FROM
                price AS pr
                    INNER JOIN
                (SELECT 
                    product_id, MAX(created_at) AS maxDate
                FROM
                    price
                GROUP BY product_id) AS pr_2 ON pr.product_id = pr_2.product_id
                    AND pr.created_at = pr_2.maxDate
                    INNER JOIN product p on pr.product_id = p.id
                WHERE pr.amount IS NOT NULL
                AND pr.product_id IN (?)
                    ';

            $stmt = $this->getEntityManager()->getConnection()
                ->executeQuery($sql, [$ids], [Connection::PARAM_INT_ARRAY]);
            $result = $stmt->fetchAll();

            $data = [];

            foreach ($result as $price) {
                $data[$price['product_id']] = [
                    'amount' => $price['amount'],
                    'discount_amount' => $price['discount_amount'],
                    'created_at' => new \DateTime($price['created_at'])
                ];
            }

            return $data;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getLastPrice(Product $product)
    {
        try {
            $sql = '
            SELECT 
                pr.*, IF(p.discount IS NULL, NULL, (100 - p.discount)/100 * pr.amount) AS discount_amount
            FROM
                price AS pr
                    INNER JOIN
                (SELECT 
                    product_id, MAX(created_at) AS maxDate
                FROM
                    price
                GROUP BY product_id) AS pr_2 ON pr.product_id = pr_2.product_id
                    AND pr.created_at = pr_2.maxDate
                    INNER JOIN product p on pr.product_id = p.id
                WHERE pr.amount IS NOT NULL
                AND pr.product_id = :productId
                ';

            $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
            $stmt->execute([
                ':productId' => $product->getId()
            ]);

            $result = $stmt->fetchAll();

            if (!empty($result)) {
                $result = [
                    'amount' => $result[0]['amount'],
                    'discount_amount' => $result[0]['discount_amount'],
                    'created_at' => new \DateTime($result[0]['created_at'])
                ];
            }

            return $result;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getLatestPricesByProduct(Product $product)
    {
        if (!$product->getId()) {
            return null;
        }

        return $this->createQueryBuilder('p')
            ->join('p.product', 'prod')
            ->where('prod.id = :product')
            ->setParameter('product', $product)
            ->orderBy('p.createdAt', 'desc')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }
}
