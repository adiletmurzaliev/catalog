<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getAllDql(bool $partner)
    {
        return $this->createQueryBuilder('p')
            ->where('p.partner = :partner')
            ->setParameter('partner', $partner)
            ->orderBy('p.createdAt', 'desc')
            ->getQuery();
    }

    public function getByCategoryDQL(Category $category)
    {
        return $this->createQueryBuilder('p')
            ->join('p.categories', 'c')
            ->where('c.id = :category')
            ->setParameter('category', $category)
            ->orderBy('p.createdAt', 'desc')
            ->getQuery();
    }

    public function getByTitle(string $title)
    {
        try {
            return $this->createQueryBuilder('p')
                ->where('p.title = :title')
                ->setParameter('title', $title)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
