<?php

namespace App\Admin;


use App\Entity\Category;
use App\Entity\Price;
use App\Entity\Product;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProductAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->configurationPool->getContainer();
        $twig = $container->get('twig');
        $priceRepository = $container->get('doctrine.orm.entity_manager')->getRepository(Price::class);
        $vichHelper = $container->get('vich_uploader.templating.helper.uploader_helper');

        /** @var Product $product */
        $product = $this->getSubject();
        $image = $vichHelper->asset($product, 'imageFile');
        $imageUrl = $product->getImageUrl();

        $formMapper
            ->with('Характеристики товара', ['class' => 'col-md-7'])
            ->add('title', TextType::class, ['label' => 'Название'])
            ->add('description', TextareaType::class, ['label' => 'Описание'])
            ->add('imageFile', FileType::class, [
                'label' => 'Фото',
                'required' => false
            ])
            ->add('imageUrl', TextType::class, [
                'label' => 'URL изображение',
                'required' => false
            ])
            ->add('categories', EntityType::class, [
                'label' => 'Категории',
                'class' => Category::class,
                'multiple' => true,
                'by_reference' => false,
            ])
            ->add('partner', CheckboxType::class, [
                'label' => 'Партнерский товар',
                'required' => false
            ])
            ->end()
            ->with('Цены и скидки', ['class' => 'col-md-5'])
            ->add('price', NumberType::class, [
                'label' => 'Новая цена',
                'required' => false
            ])
            ->add('discount', IntegerType::class, [
                'label' => 'Скидка (%)',
                'required' => false
            ])
            ->end();

        if ($image) {
            $formMapper->addHelp(
                'imageFile',
                "<img src='{$image}' alt='{$product->getTitle()}' style='width: 100px;'><hr>"
            );
        }

        if ($imageUrl) {
            $formMapper->addHelp(
                'imageUrl',
                "<img src='{$imageUrl}' alt='{$product->getTitle()}' style='width: 100px;'><hr>"
            );
        }

        if ($this->isCurrentRoute('edit')) {
            $formMapper->addHelp(
                'price',
                $twig->render('admin/last_prices.html.twig', [
                    'productPrices' => $priceRepository->getLatestPricesByProduct($product)
                ])
            );
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('description')
            ->add('categories')
            ->add('discount')
            ->add('partner');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['label' => 'ID'])
            ->addIdentifier('title', null, ['label' => 'Название'])
            ->addIdentifier('description', null, ['label' => 'Описание'])
            ->addIdentifier('categories', null, ['label' => 'Категории'])
            ->addIdentifier('discount', null, ['label' => 'Скидка (%)'])
            ->addIdentifier('partner', null, ['label' => 'Партнерский товар'])
            ->add('imageUrl', null, ['template' => 'admin/list_imageUrl.html.twig', 'label' => 'URL изоражение'])
            ->add('image', null, ['template' => 'admin/list_image.html.twig', 'label' => 'Фото']);
    }
}