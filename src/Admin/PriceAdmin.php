<?php

namespace App\Admin;


use App\Entity\Product;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class PriceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Цены на товары', ['class' => 'col-md-6'])
                ->add('amount', NumberType::class, ['label' => 'Цена'])
                ->add('product', EntityType::class, [
                    'label' => 'Товар',
                    'class' => Product::class,
                    'choice_label' => 'shortTitle'
                ])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('amount')
            ->add('product')
            ->add('createdAt');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['label' => 'ID'])
            ->addIdentifier('amount', null, ['label' => 'Цена'])
            ->addIdentifier('product', null, ['label' => 'Товар'])
            ->addIdentifier('createdAt', null, ['label' => 'Дата создания']);
    }
}