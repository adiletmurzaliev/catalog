<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    const FACEBOOK = 'facebook';
    const VK = 'vkontakte';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $facebookId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $vkId;


    public function __construct()
    {
        parent::__construct();
    }

    public function getSocialId(string $network): ?string
    {
        switch ($network) {
            case User::FACEBOOK:
                return $this->facebookId;
            case User::VK:
                return $this->vkId;
            default:
                return null;
        }
    }

    public function setSocialId(string $network, string $id)
    {
        switch ($network) {
            case User::FACEBOOK:
                $this->facebookId = $id;
                return $this;
            case User::VK:
                $this->vkId = $id;
                return $this;
            default:
                return $this;
        }
    }
}
