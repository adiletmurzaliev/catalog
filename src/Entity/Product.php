<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Введите название товара")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Введите название товара")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $imageUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @Assert\File(
     *     maxSize="3M",
     *     mimeTypes={"image/jpeg", "image/png"},
     *     mimeTypesMessage="Выбрано изображение с недопустимым форматом (валидные: .jpeg, .png)",
     *     maxSizeMessage="Размер изображения слишком большой"
     * )
     */
    private $imageFile;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer", message="Введите целое число")
     * @Assert\Range(
     *      min = 1,
     *      max = 100,
     *      minMessage = "Размер скидки не должен быть меньше {{ limit }} %",
     *      maxMessage = "Размер скидки не должен быть больше {{ limit }} %"
     * )
     */
    private $discount;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $partner;

    /**
     * @var float
     */
    private $price;

    /**
     * @var Price[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Price", mappedBy="product", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $prices;

    /**
     * @var Category[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", mappedBy="products", cascade={"persist", "remove"})
     */
    private $categories;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->prices = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->setPartner(false);
    }

    /**
     * @Assert\Callback()
     *
     * @param ExecutionContextInterface $context
     */
    public function isCategoriesValid(ExecutionContextInterface $context)
    {
        if ($this->categories->count() === 0) {
            $context->buildViolation('Выберите хотя бы одну категорию')
                ->atPath('categories')
                ->addViolation();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getShortTitle(): ?string
    {
        if ($this->title && mb_strlen($this->title) > 120) {
            return substr($this->title, 0, 120) . '...';
        }

        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function setImageFile(?File $image = null): self
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(?int $discount): Product
    {
        $this->discount = $discount;

        return $this;
    }

    public function setPartner(bool $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    public function isPartner(): bool
    {
        return $this->partner;
    }

    /**
     * @return Collection|Price[]
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(Price $price): self
    {
        if (!$this->prices->contains($price)) {
            $this->prices[] = $price;
            $price->setProduct($this);
        }

        return $this;
    }

    public function removePrice(Price $price): self
    {
        if ($this->prices->contains($price)) {
            $this->prices->removeElement($price);
            // set the owning side to null (unless already changed)
            if ($price->getProduct() === $this) {
                $price->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addProduct($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeProduct($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle() ?: '';
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        $priceObj = new Price();
        $priceObj->setAmount($price);
        $this->addPrice($priceObj);

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }
}
