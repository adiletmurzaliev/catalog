<?php

namespace App\DataFixtures;

use App\Entity\Price;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PriceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $productRefs = ProductFixtures::getProducts();

        foreach ($productRefs as $productRef) {
            /** @var Product $product */
            $product = $this->getReference($productRef);

            for ($i = 1; $i >= 0; $i--) {
                switch ($rnd = rand(1, 7)) {
                    case $rnd <= 6:
                        $price = new Price();
                        $price
                            ->setAmount(rand(1000, 10000))
                            ->setProduct($product)
                            ->setCreatedAt(new \DateTime("-{$i} days"));

                        if (floor(rand(0, 2))) {
                            $product->setDiscount(rand(5, 20));
                        }

                        $manager->persist($price);
                        break;
                    case $rnd === 7:
                        $price = new Price();
                        $price
                            ->setAmount(null)
                            ->setProduct($product)
                            ->setCreatedAt(new \DateTime("-{$i} days"));
                        $manager->persist($price);
                        break;
                    case $rnd === 8:
                        break;
                }
            }
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            ProductFixtures::class
        ];
    }
}
