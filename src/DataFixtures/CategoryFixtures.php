<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    const CATEGORY_PHONE = 'Телефоны';
    const CATEGORY_TABLET = 'Планшеты';
    const CATEGORY_OTHER = 'Другое';

    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setTitle(self::CATEGORY_PHONE);
        $manager->persist($category);
        $this->addReference(self::CATEGORY_PHONE, $category);

        $category = new Category();
        $category->setTitle(self::CATEGORY_TABLET);
        $manager->persist($category);
        $this->addReference(self::CATEGORY_TABLET, $category);

        $category = new Category();
        $category->setTitle(self::CATEGORY_OTHER);
        $manager->persist($category);
        $this->addReference(self::CATEGORY_OTHER, $category);

        $manager->flush();
    }
}
