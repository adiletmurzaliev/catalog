<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('admin')
            ->setEmail('admin@mail.ru')
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true);
        $manager->persist($user);

        $manager->flush();
    }
}
