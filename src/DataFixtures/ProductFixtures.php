<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public static $products = [];

    public function load(ObjectManager $manager)
    {
        /** @var Category $categoryPhone */
        $categoryPhone = $this->getReference(CategoryFixtures::CATEGORY_PHONE);
        /** @var Category $categoryTablet */
        $categoryTablet = $this->getReference(CategoryFixtures::CATEGORY_TABLET);
        /** @var Category $categoryOther */
        $categoryOther = $this->getReference(CategoryFixtures::CATEGORY_OTHER);

        $this->clearImages();
        $data = $this->generateData();

        foreach ($data as $item) {
            $product = new Product();
            $product
                ->setTitle($item['title'])
                ->setDescription($item['description'])
                ->setImageFile(new UploadedFile($item['image'], $item['productId'], null, null, null, true));

            switch (rand(1, 4)) {
                case 1:
                    $product->addCategory($categoryPhone);
                    break;
                case 2:
                    $product->addCategory($categoryTablet);
                    break;
                case 3:
                    $product->addCategory($categoryOther);
                    break;
                case 4:
                    $product
                        ->addCategory($categoryPhone)
                        ->addCategory($categoryTablet);
                    break;
            }

            $manager->persist($product);

            self::$products[] = $item['productId'];
            $this->addReference($item['productId'], $product);
        }

        $manager->flush();
    }

    private function generateData(): array
    {
        $fixturesPath = __DIR__ . '/../../public/images/fixtures/';
        $fixturesImages = glob($fixturesPath . '*');
        $data = [];

        for ($i = 1; $i <= 30; $i++) {
            $rndImage = $fixturesImages[array_rand($fixturesImages)];
            copy($rndImage, $fixturesPath . "product{$i}.png");

            $data[] = [
                'title' => "Товар {$i}",
                'description' => "Описание к товару {$i}",
                'image' => $fixturesPath . "product{$i}.png",
                'productId' => "product{$i}"
            ];

        }

        return $data;
    }

    private function clearImages()
    {

        $files = glob(__DIR__ . '/../../public/uploads/images/products/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
    }

    public static function getProducts()
    {
        return self::$products;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}
