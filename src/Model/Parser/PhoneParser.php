<?php

namespace App\Model\Parser;

use App\Entity\Price;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Curl\Curl;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Stopwatch\Stopwatch;

class PhoneParser
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository,
        ObjectManager $manager
    )
    {
        $this->productRepository = $productRepository;
        $this->manager = $manager;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @throws \Exception
     */
    public function updateData()
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('executionTime');
        $data = $this->parseHtml();

        foreach ($data as $item) {
            /** @var Product $product */
            $product = $this->productRepository->getByTitle($item['title']);
            $category = $this->categoryRepository->getByTitle('Телефоны');
            $price = new Price();
            $price->setAmount($item['price']);

            if (!$product) {
                $product = new Product();
                $product
                    ->setTitle($item['title'])
                    ->setDescription($item['title'])
                    ->setPartner(true)
                    ->addCategory($category);
                $product->addPrice($price);
            }

            $product->setImageUrl($item['img']);

            $this->manager->persist($product);
            $this->manager->flush();
        }

        echo $stopwatch->stop('executionTime')->getDuration();
    }

    /**
     * @throws \Exception
     */
    private function parseHtml()
    {
        $document = $this->loadHtml('http://enter.kg/sotovye_telefony_bishkek');

        $xpath = new \DOMXPath($document);
        $items = $xpath->query('//div[contains(@class,"product vm-col vm-col-1")]');

        $products = [];

        foreach ($items as $item) {
            $img = $xpath->query('.//img', $item)[0]->getAttribute('src');
            $title = $xpath->query('.//span[@class="prouct_name"]', $item)[0]->firstChild->textContent;
            $price = $xpath->query('.//span[@class="price"]', $item)[0]->textContent;

            $products[] = [
                'title' => $title,
                'price' => $this->formatPrice($price),
                'img' => 'http://enter.kg' . $img
            ];
        }

        return $products;
    }

    /**
     * @param string $url
     * @return \DOMDocument
     * @throws \ErrorException
     */
    private function loadHtml(string $url): \DOMDocument
    {
        $curl = new Curl();
        $curl->setHeader(
            'user-agent',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
        );
        $curl->get($url);

        if ($curl->error) {
            throw new \Exception($curl->errorMessage, $curl->errorCode);
        } else {
            $document = new \DOMDocument();
            libxml_use_internal_errors(true);
            $document->loadHTML($curl->response);
            libxml_use_internal_errors(false);
            return $document;
        }
    }

    /**
     * @param string $text
     * @return float
     */
    private function formatPrice(string $text)
    {
        return (float)trim(explode('Сом', $text)[0]);
    }
}