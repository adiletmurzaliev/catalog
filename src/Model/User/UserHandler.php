<?php
/**
 * Created by PhpStorm.
 * User: adilet
 * Date: 10/8/18
 * Time: 11:36 AM
 */

namespace App\Model\User;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        SessionInterface $session,
        UserRepository $userRepository
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->userRepository = $userRepository;
    }

    public function makeUserSession(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->tokenStorage->setToken($token);
        $this->session->set('_security_main', serialize($token));
    }

}