$(function () {
    let getData = function () {
        $.get(urlAjax, data, function (response) {
            console.log(response.message);
            console.log(response.productsBlock);
            history.pushState(null, null, url + '?' + $.param(data));
            let productsBlock = $('#productsBlock');
            if (response.productsBlock) {
                productsBlock.empty();
                productsBlock.append(response.productsBlock);
                bindPageLinkEvents();
            }
        });
    };

    let bindPageLinkEvents = function () {
        $('.page-link').on('click', function (e) {
            e.preventDefault();

            let activePage = parseInt($('.page-item.active').text());

            if ($(this).attr('rel') !== undefined) {
                data.page = ($(this).attr('rel') === 'prev') ? activePage - 1 : activePage + 1;
                console.log($(this).attr('rel'));
            } else {
                data.page = parseInt($(this).text());
            }

            getData();
        });
    };

    let data = {};
    bindPageLinkEvents();

    $(window).on('popstate', function (e) {
        window.location.replace(e.currentTarget.location.href);
    });
});